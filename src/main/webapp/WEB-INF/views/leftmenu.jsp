<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@ page contentType="text/html; charset=ISO-8859-1"%>

<div class="col-sm-3 col-md-2 sidebar">
	<ul class="nav nav-sidebar">
		
		<li class="${pageContext.request.requestURI eq '/ppro/WEB-INF/views/welcome.jsp' ? ' active' : ''}">
			<a href="<c:url value="/welcome" />">Welcome <span class="sr-only">(current)</span></a>
		</li>
		
		<li class="${pageContext.request.requestURI eq '/ppro/WEB-INF/views/showSongs.jsp' ? ' active' : ''}">
			<a href='<c:url value="/songs" />'>Playlist</a>
		</li>
		
		<li class="${pageContext.request.requestURI eq '/ppro/WEB-INF/views/showNews.jsp' ? ' active' : ''}">
			<a href='<c:url value="/news" />'>News</a>
		</li>
		
		<li class="${pageContext.request.requestURI eq '/ppro/WEB-INF/views/showConcerts.jsp' ? ' active' : ''}">
			<a href='<c:url value="/concerts" />'>Concerts</a>
		</li>
	
		<li class="${pageContext.request.requestURI eq '/ppro/WEB-INF/views/showTasks.jsp' ? ' active' : ''}">
			<a href='<c:url value="/tasks" />'>Tasks</a>
		</li>
		
		<li class="${pageContext.request.requestURI eq '/ppro/WEB-INF/views/showPersonalTasks.jsp' ? ' active' : ''}">
			<a href='<c:url value="/personalTasks" />'>Personal Tasks</a>
		</li>
		
		<li class="${pageContext.request.requestURI eq '/ppro/WEB-INF/views/showUsers.jsp' ? ' active' : ''}">
			<a href='<c:url value="/users" />'>Users</a>
		</li>
		
	</ul>
</div>