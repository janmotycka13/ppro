<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@ page contentType="text/html; charset=ISO-8859-1"%>
<html>

<head>
<%@ include file="head.jsp"%>

<title>Users</title>
</head>

<body>
	<%@ include file="topmenu.jsp"%>

	<div class="container-fluid">
		<div class="row">
			<%@ include file="leftmenu.jsp"%>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">Users</h1>
				
				<div class="row">
					<div class="col-md-12">
						<a href='<c:url value="/users/csv" />' class="btn btn-info"><i class="fa fa-download"></i>&nbsp;Export do CSV</a>
					</div>
				</div>

				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th class="col-md-1">Id</th>
								<th class="col-md-1">Username</th>
								<th class="col-md-1">Name</th>
								<th class="col-md-1">Surname</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="user" items="${users}">
								<tr>
									<td><c:out value="${user.id}" /></td>
									<td><c:out value="${user.username}" /></td>
									<td><c:out value="${user.name}" /></td>
									<td><c:out value="${user.surname}" /></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<p>Number of records: ${numberUsers}</p>
			</div>
		</div>
	</div>
</body>
</html>