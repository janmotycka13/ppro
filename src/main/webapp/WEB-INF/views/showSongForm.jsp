<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html; charset=ISO-8859-1"%>
<html>

<head>
<%@ include file="head.jsp"%>

<title>Song</title>
</head>

<body>
	<%@ include file="topmenu.jsp"%>

	<div class="container-fluid">
		<div class="row">
			<%@ include file="leftmenu.jsp"%>
			
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">Add/edit song</h1>

				<c:url value="/songs/song/addEdit" var="url" />
				<form:form method="post" modelAttribute="songForm" action="${url}">

					<div class="row">
						<div class="col-md-4 form-group">
							<form:label path="author">Author:</form:label>
							<form:input path="author" type="text" class="form-control" />
							<c:set var="authorHasBindError">
								<form:errors path="author" />
							</c:set>
							<c:if test="${not empty authorHasBindError}">
								<div class="alert alert-danger">
									<i class="fa fa-ban"></i>&nbsp;${authorHasBindError}
								</div>
							</c:if>
						</div>
					</div>

					<div class="row">
						<div class="col-md-4 form-group">
							<form:label path="name">Name:</form:label>
							<form:input path="name" type="text" class="form-control" />
							<c:set var="nameHasBindError">
								<form:errors path="name" />
							</c:set>
							<c:if test="${not empty nameHasBindError}">
								<div class="alert alert-danger">
									<i class="fa fa-ban"></i>&nbsp;${nameHasBindError}
								</div>
							</c:if>
						</div>
					</div>
					<form:input path="id" type="hidden" />

					<button type="submit" class="btn btn-primary">
						<i class="fa fa-save"></i>&nbsp;Save
					</button>

				</form:form>
			</div>
		</div>
	</div>
</body>
</html>