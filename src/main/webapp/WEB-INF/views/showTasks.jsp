<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page session="false"%>
<%@ page contentType="text/html; charset=ISO-8859-1"%>
<html>
<head>
<%@ include file="head.jsp"%>
<title>Tasks</title>

<style type="text/css">
table.table-hover tbody tr:hover {
	background-color: #e8ebef;
}
</style>

</head>
<body>

<%@ include file="topmenu.jsp"%>

	<div class="container-fluid">
		<div class="row">
			<%@ include file="leftmenu.jsp"%>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">Tasks</h1>
				<div class="row">
					<div class="col-md-12">
						
						<a href='<c:url value="/tasks/task/0" />'
							class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;Add</a> 
							
						<a href='<c:url value="/tasks/csv" />' class="btn btn-info"><i
						class="fa fa-download"></i>&nbsp;Export do CSV</a>
							
					</div>
				</div>
				
				<div class="table-responsive">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th class="col-md-1">Id</th>
								<th class="col-md-1">Name</th>
								<th class="col-md-2">Description</th>
								<th class="col-md-2">Place</th>
								<th class="col-md-2">Date of Entry</th>
								<th class="col-md-2">Date of Completion</th>
								<th class="col-md-2">Action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="task" items="${tasks}">
								<tr>
									<td><c:out value="${task.idTask}" /></td>
									<td><c:out value="${task.name}" /></td>
									<td><c:out value="${task.description}" /></td>
									<td><c:out value="${task.place}" /></td>
									<td><fmt:formatDate value="${task.dateOfEntry}" pattern="dd. MM. YYYY"/></td>
									<td><fmt:formatDate value="${task.dateOfCompletion}" pattern="dd. MM. YYYY"/></td>
									
									<td class="text-center">
										<a class="btn btn-warning" href="<c:url value="/tasks/task/${task.idTask}"></c:url>">
											<i class="fa fa-pencil-square-o"></i>&nbsp;Edit</a>&nbsp; 
										
										<a onclick="return confirm('Do you really want to delete this item?');"
											href="<c:url value="/tasks/task/delete/${task.idTask}"></c:url>"
											class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;Delete</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<p>Number of records: ${numberTasks}</p>
				</div>
			</div>
		</div>
	</div>

</body>
</html>