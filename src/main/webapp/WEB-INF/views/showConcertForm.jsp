<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html; charset=ISO-8859-1"%>
<html>
<head>
<%@ include file="head.jsp"%>
	
<title>Concert</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
</head>
<body>
<%@ include file="topmenu.jsp"%>

	<div class="container-fluid">
		<div class="row">
		<%@ include file="leftmenu.jsp"%>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">Add/edit concert</h1>

				<c:url value="/concerts/concert/addEdit" var="url" />
				<form:form method="post" modelAttribute="concertForm" action="${url}">

					<div class="row">
						<div class="col-md-4 form-group">
							<form:label path="date">Date:</form:label>
							<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
							<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
							<script>
								$(function() {
									$("#datepicker").datepicker();
									$("#anim").on("change", function() {
										$("#datepicker").datepicker();
									});
								});
							</script>

							<input type="text" id="datepicker" name="date" size="30">

							<c:set var="dateHasBindError">
								<form:errors path="date" />
							</c:set>
							<c:if test="${not empty dateHasBindError}">
								<div class="alert alert-danger">
									<i class="fa fa-ban"></i>&nbsp;${dateHasBindError}
								</div>
							</c:if>
						</div>
					</div>

					<div class="row">
						<div class="col-md-4 form-group">
							<form:label path="place">Place:</form:label>
							<form:input path="place" type="text" class="form-control" />
							<c:set var="placeHasBindError">
								<form:errors path="place" />
							</c:set>
							<c:if test="${not empty placeHasBindError}">
								<div class="alert alert-danger">
									<i class="fa fa-ban"></i>&nbsp;${placeHasBindError}
								</div>
							</c:if>
						</div>
					</div>

					<div class="row">
						<div class="col-md-4 form-group">
							<form:label path="description">Description:</form:label>
							<form:input path="description" type="text" class="form-control" />
							<c:set var="descriptionHasBindError">
								<form:errors path="description" />
							</c:set>
							<c:if test="${not empty descriptionHasBindError}">
								<div class="alert alert-danger">
									<i class="fa fa-ban"></i>&nbsp;${descriptionHasBindError}
								</div>
							</c:if>
						</div>
					</div>

					<form:input path="idConcerts" type="hidden" />

					<button type="submit" class="btn btn-primary">
						<i class="fa fa-save"></i>&nbsp;Save
					</button>

				</form:form>
			</div>
		</div>
	</div>
</body>
</html>