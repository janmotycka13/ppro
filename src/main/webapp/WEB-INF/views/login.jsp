<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html; charset=ISO-8859-1"%>
<html>

<head>
<%@ include file="head.jsp"%>

<title>Login</title>

<style type="text/css">
.form-signin {
	max-width: 330px;
	margin: auto;
}

 .form-signin input[type="text"], .form-signin input[type="password"] {
 	margin-bottom: 15px;
 }
</style>

</head>

<body>
	<div class="container">
		<form:form method="post" modelAttribute="loginForm" class="form-signin">
			<h2 class="form-signin-heading">Please sign in</h2>
			
			<form:label path="username" class="sr-only">Username</form:label>
			<form:input path="username" type="text" class="form-control" placeholder="Username" />
			<c:set var="usernameHasBindError">
				<form:errors path="username" />
			</c:set>
			<c:if test="${not empty usernameHasBindError}">
				<div class="alert alert-danger">
					<i class="fa fa-ban"></i>&nbsp;${usernameHasBindError}
				</div>
			</c:if>

			<form:label path="password" class="sr-only">Password:</form:label>
			<form:input path="password" type="password" class="form-control" placeholder="Password" />
			<c:set var="passwordHasBindError">
				<form:errors path="password" />
			</c:set>
			<c:if test="${not empty passwordHasBindError}">
				<div class="alert alert-danger">
					<i class="fa fa-ban"></i>&nbsp;${passwordHasBindError}
				</div>
			</c:if>
			
			<c:set var="formHasBindError">
				<form:errors />
			</c:set>
			<c:if test="${not empty formHasBindError}">
				<div class="alert alert-danger">
					<i class="fa fa-ban"></i>&nbsp;${formHasBindError}
				</div>
			</c:if>

			<input type="submit" value="Sign in" class="btn btn-lg btn-primary btn-block" />
		</form:form>
	</div>
</body>
</html>