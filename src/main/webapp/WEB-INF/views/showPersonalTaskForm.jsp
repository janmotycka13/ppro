<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html; charset=ISO-8859-1"%>
<html>
<head>
<%@ include file="head.jsp"%>


<title>Task</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->

</head>
<body>
	<%@ include file="topmenu.jsp"%>
	<div class="container-fluid">
		<div class="row">
			<%@ include file="leftmenu.jsp"%>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">Add/edit personal task</h1>

				<c:url value="/personalTasks/personalTask/addEdit" var="url" />
				<form:form method="post" modelAttribute="personalTaskForm" action="${url}">
					
					<div class="row">
						<div class="col-md-4 form-group">
							<form:label path="date">Date:</form:label>
							<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
							<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
							<script>
								$(function() {
									$("#datepicker").datepicker();
									$("#anim").on("change", function() {
										$("#datepicker").datepicker();
									});
								});
							</script>

							<input type="text" id="datepicker" name="date" size="30">

							<c:set var="dateHasBindError">
								<form:errors path="date" />
							</c:set>
							<c:if test="${not empty dateHasBindError}">
								<div class="alert alert-danger">
									<i class="fa fa-ban"></i>&nbsp;${dateHasBindError}
								</div>
							</c:if>
						</div>
					</div>
					

					<div class="row">
						<div class="col-md-4 form-group">
							<form:label path="taskTo">Task to:</form:label>
							<form:input path="taskTo" type="text" class="form-control" />
							<c:set var="taskToHasBindError">
								<form:errors path="taskTo" />
							</c:set>
							<c:if test="${not empty taskToHasBindError}">
								<div class="alert alert-danger">
									<i class="fa fa-ban"></i>&nbsp;${taskToHasBindError}
								</div>
							</c:if>
						</div>
					</div>
					
					
					
					<div class="row">
						<div class="col-md-4 form-group">
							<form:label path="message">Message:</form:label>
							<form:input path="message" type="text" class="form-control" />
							<c:set var="messageHasBindError">
								<form:errors path="message" />
							</c:set>
							<c:if test="${not empty messageHasBindError}">
								<div class="alert alert-danger">
									<i class="fa fa-ban"></i>&nbsp;${messageHasBindError}
								</div>
							</c:if>
						</div>
					</div>
					
					<form:input path="idPersonalTasks" type="hidden" />

					<button type="submit" class="btn btn-primary">
						<i class="fa fa-save"></i>&nbsp;Save
					</button>

				</form:form>
			</div>
		</div>
	</div>
</body>
</html>