<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@ page contentType="text/html; charset=ISO-8859-1"%>
<html>
<head>
<%@ include file="head.jsp"%>

<title>News</title>

</head>

<body>
<%@ include file="topmenu.jsp"%>
	<div class="container-fluid">
		<div class="row">
		<%@ include file="leftmenu.jsp"%>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">News</h1>
				
				<div class="row">
					<div class="col-md-12">
						<a href='<c:url value="/news/oneNews/0" />'
						class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;Add</a> 
							
						<a href='<c:url value="/news/csv" />' class="btn btn-info"><i
						class="fa fa-download"></i>&nbsp;Export do CSV</a>
					</div>
				</div>

				<c:forEach var="oneNews" items="${news}">
					<div class="row">
						<div class="col-md-12">
							<h3 class="sub-header">
								<c:out value="${oneNews.title}" />
							</h3>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<p>
								<c:out value="${oneNews.content}" />
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<a class="btn btn-warning"
								href="<c:url value="/news/oneNews/${oneNews.idNews}"></c:url>"><i
								class="fa fa-pencil-square-o"></i>&nbsp;Edit</a>&nbsp; 
								
								<a onclick="return confirm('Do you really want to delete this item?');"
								href="<c:url value="/news/oneNews/delete/${oneNews.idNews}"></c:url>"
								class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;Delete</a>
						</div>
					</div>
				</c:forEach>
				
				<br><p>Number of records: ${numberNews} </p>
			</div>
			
		</div>
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>