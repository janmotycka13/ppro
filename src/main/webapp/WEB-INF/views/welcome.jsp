<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@ page contentType="text/html; charset=ISO-8859-1"%>
<html>
<head>
<%@ include file="head.jsp"%>
<title>Welcome</title>
</head>
<body>
	<%@ include file="topmenu.jsp"%>

	<div class="container-fluid">
		<div class="row">
			<%@ include file="leftmenu.jsp"%>

			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">Welcome, ${name}, in administration of Green Day</h1>

				<div class="row placeholders">

					<div class="col-xs-6 col-sm-3 placeholder">
						<img
							src="http://cdnrockol.rockolcomsrl.netdna-cdn.com/ZcdS11daVyhuOaRueJWrwb_HHwk=/200x200/smart/http%3A%2F%2Fwww.rockol.it%2Fimg%2Ffoto%2Fupload%2Fgreenday77.jpg"
							width="200" height="200" class="img-responsive"
							alt="Generic placeholder thumbnail">
						<h3>Billie Joe Armstrong</h3>
						<p>lead vocals, major (solo) guitar, piano</p>
					</div>

					<div class="col-xs-6 col-sm-3 placeholder">
						<img
							src="http://cdn2.newsok.biz/cache/sq200-85b4146485126c1b8ae3af321c191625.jpg"
							width="200" height="200" class="img-responsive"
							alt="Generic placeholder thumbnail">
						<h3>Mike Dirnt</h3>
						<p>bass guitar, rhythm guitar, backing vocals</p>
					</div>

					<div class="col-xs-6 col-sm-3 placeholder">
						<img
							src="http://images6.fanpop.com/image/polls/1119000/1119324_1348015642135_full.jpg"
							width="200" height="200" class="img-responsive"
							alt="Generic placeholder thumbnail">
						<h3>Tre Cool</h3>
						<p>drums, backing vocals</p>
					</div>

					<div class="col-xs-6 col-sm-3 placeholder">
						<img
							src="http://www.play.cz/wp-content/uploads/2014/12/jason_white_green_day_oficial_web.jpg"
							width="200" height="200" class="img-responsive"
							alt="Generic placeholder thumbnail">
						<h3>Jason White</h3>
						<p>rhythm guitar, backing vocals</p>
					</div>

				</div>
				
				<h2 class="sub-header">About</h2>
				<p>Green Day is an American punk rock band formed in 1986 by
					lead vocalist and guitarist Billie Joe Armstrong and bassist Mike
					Dirnt. For much of the group's career, the band has been a trio
					with drummer Tre Cool, who replaced former drummer John Kiffmeyer
					in 1990 prior to the recording of the band's second studio album,
					Kerplunk (1991). Guitarist Jason White, who has worked with the
					band as a touring member since 1999, was an official member from
					2012 to 2016.</p>
				<p>Green Day was originally part of the punk scene at the DIY
					924 Gilman Street club in Berkeley, California. The band's early
					releases were with the independent record label Lookout! Records.
					In 1994, its major label debut Dookie (released through Reprise
					Records) became a breakout success and eventually shipped over 10
					million copies in the U.S.[3] Green Day was widely credited,
					alongside fellow California punk bands Sublime,[4] Bad Religion,
					The Offspring, Blink-182, and Rancid, with popularizing and
					reviving mainstream interest in punk rock in the United States.</p>
				<p>Green Day's three follow-up albums, Insomniac (1995), Nimrod
					(1997), and Warning (2000) did not achieve the massive success of
					Dookie, though they were still successful, with Insomniac and
					Nimrod reaching double platinum and Warning achieving platinum
					status.[8] The band's rock opera, American Idiot (2004), reignited
					the band's popularity with a younger generation, selling six
					million copies in the U.S.[8] The band's eighth studio album, 21st
					Century Breakdown, was released in 2009 and achieved the band's
					best chart performance to date.[9] 21st Century Breakdown was
					followed up by a trilogy of albums called iUno!, iDos!, and iTre!,
					which were released in September, November and December 2012
					respectively.[10] The band's twelfth studio album, Revolution Radio
					was released on October 7, 2016[2] and debuted at number one on the
					Billboard 200, their third album to do so.</p>
				<p>Green Day has sold more than 85 million records worldwide.The
					group has won five Grammy Awards: Best Alternative Album for
					Dookie, Best Rock Album for American Idiot, Record of the Year for
					"Boulevard of Broken Dreams", Best Rock Album for the second time
					for 21st Century Breakdown and Best Musical Show Album for American
					Idiot: The Original Broadway Cast Recording. In 2010, a stage
					adaptation of American Idiot debuted on Broadway. The musical was
					nominated for three Tony Awards: Best Musical, Best Scenic Design
					and Best Lighting Design, losing only the first. Also in 2010,
					Green Day was ranked no. 91 in the VH1 list of the "100 Greatest
					Artists of All Time". On April 18, 2015, the band was inducted into
					the Rock and Roll Hall of Fame as a part of the 2015 class in their
					first year of eligibility.</p>

				<div class="text-center">
					<button type="button" class="btn btn-info btn-lg center-block"
						data-toggle="modal" data-target="#myModal">Green Day official website</button>

					<!-- Modal -->
					<div class="modal fade" id="myModal" role="dialog">
						<div class="modal-dialog modal-sm">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Leaving?</h4>
								</div>
								<div class="modal-body">
									<p>Do you really want to leave this website?</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
									<a href="https://www.greenday.com" class="btn btn-info" role="button">Yes</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="text-center">
					<h2 class="sub-header">Videos</h2>

					<iframe width="420" height="315"
						src="https://www.youtube.com/embed/NUTGr5t3MoY"> </iframe>

					<iframe width="420" height="315"
						src="https://www.youtube.com/embed/Soa3gO7tL-c"> </iframe>

					<iframe width="420" height="315"
						src="https://www.youtube.com/embed/Ee_uujKuJMI"> </iframe>

					<iframe width="420" height="315"
						src="https://www.youtube.com/embed/FNKPYhXmzoE"> </iframe>

				</div>				
			</div>
		</div>
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>