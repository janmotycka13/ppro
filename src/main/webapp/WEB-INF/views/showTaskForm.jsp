<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html; charset=ISO-8859-1"%>
<html>

<head>
<%@ include file="head.jsp"%>

<title>Task</title>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
</head>

<body>
	<%@ include file="topmenu.jsp"%>

	<div class="container-fluid">
		<div class="row">
			<%@ include file="leftmenu.jsp"%>
			
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">Add/edit task</h1>

				<c:url value="/tasks/task/addEdit" var="url" />
				<form:form method="post" modelAttribute="taskForm" action="${url}">

					<div class="row">
						<div class="col-md-4 form-group">
							<form:label path="name">Name:</form:label>
							<form:input path="name" type="text" class="form-control" />
							<c:set var="nameHasBindError">
								<form:errors path="name" />
							</c:set>
							<c:if test="${not empty nameHasBindError}">
								<div class="alert alert-danger">
									<i class="fa fa-ban"></i>&nbsp;${nameHasBindError}
								</div>
							</c:if>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-4 form-group">
							<form:label path="description">Description:</form:label>
							<form:input path="description" type="text" class="form-control" />
							<c:set var="descriptionHasBindError">
								<form:errors path="name" />
							</c:set>
							<c:if test="${not empty descriptionHasBindError}">
								<div class="alert alert-danger">
									<i class="fa fa-ban"></i>&nbsp;${descriptionHasBindError}
								</div>
							</c:if>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-4 form-group">
							<form:label path="place">Place:</form:label>
							<form:input path="place" type="text" class="form-control" />
							<c:set var="placeHasBindError">
								<form:errors path="place" />
							</c:set>
							<c:if test="${not empty placeHasBindError}">
								<div class="alert alert-danger">
									<i class="fa fa-ban"></i>&nbsp;${placeHasBindError}
								</div>
							</c:if>
						</div>
					</div>

					<div class="row">
						<div class="col-md-4 form-group">
							<form:label path="dateOfEntry">Date of Entry:</form:label>
							<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
							<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
							<script>
								$(function() {
									$("#datepicker").datepicker();
									$("#anim").on("change", function() {
										$("#datepicker").datepicker();
									});
								});
							</script>

							<input type="text" id="datepicker" name="dateOfEntry" size="30">

							<c:set var="dateOfEntryHasBindError">
								<form:errors path="dateOfEntry" />
							</c:set>
							<c:if test="${not empty dateOfEntryHasBindError}">
								<div class="alert alert-danger">
									<i class="fa fa-ban"></i>&nbsp;${dateOfEntryHasBindError}
								</div>
							</c:if>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-4 form-group">
							<form:label path="dateOfCompletion">Date of Completion:</form:label>
							<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
							<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
							<script>
								$(function() {
									$("#datepick").datepicker();
									$("#anim").on("change", function() {
										$("#datepick").datepicker();
									});
								});
							</script>

							<input type="text" id="datepick" name="dateOfCompletion" size="30">

							<c:set var="dateOfCompletionHasBindError">
								<form:errors path="dateOfCompletion" />
							</c:set>
							<c:if test="${not empty dateOFCompletionHasBindError}">
								<div class="alert alert-danger">
									<i class="fa fa-ban"></i>&nbsp;${dateOFCompletionHasBindError}
								</div>
							</c:if>
						</div>
					</div>

					<form:input path="idTasks" type="hidden" />

					<button type="submit" class="btn btn-primary">
						<i class="fa fa-save"></i>&nbsp;Save
					</button>

				</form:form>
			</div>
		</div>
	</div>
</body>
</html>