<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html; charset=ISO-8859-1"%>
<html>

<head>
<%@ include file="head.jsp"%>


<title>News</title>

</head>
<body>
<%@ include file="topmenu.jsp"%>
	<div class="container-fluid">
		<div class="row">
			<%@ include file="leftmenu.jsp"%>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">Add/edit news</h1>

				<c:url value="/news/oneNews/addEdit" var="url" />
				<form:form method="post" modelAttribute="oneNewsForm" action="${url}">

					<div class="row">
						<div class="col-md-4 form-group">
							<form:label path="title">Title:</form:label>
							<form:input path="title" type="text" class="form-control" />
							<c:set var="titleHasBindError">
								<form:errors path="title" />
							</c:set>
							<c:if test="${not empty titleHasBindError}">
								<div class="alert alert-danger">
									<i class="fa fa-ban"></i>&nbsp;${titleHasBindError}
								</div>
							</c:if>
						</div>
					</div>

					<div class="row">
						<div class="col-md-8 form-group">
							<form:label path="content">Content:</form:label>
							<form:textarea path="content" class="form-control" rows="10"></form:textarea>
							<c:set var="contentHasBindError">
								<form:errors path="content" />
							</c:set>
							<c:if test="${not empty contentHasBindError}">
								<div class="alert alert-danger">
									<i class="fa fa-ban"></i>&nbsp;${contentHasBindError}
								</div>
							</c:if>
						</div>
					</div>

					<form:input path="idNews" type="hidden" />

					<button type="submit" class="btn btn-primary">
						<i class="fa fa-save"></i>&nbsp;Save
					</button>

				</form:form>
			</div>
		</div>
	</div>
</body>
</html>