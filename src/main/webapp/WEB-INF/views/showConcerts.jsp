<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page session="false"%>
<%@ page contentType="text/html; charset=ISO-8859-1"%>

<html>

<head>
<%@ include file="head.jsp"%>

<title>Concerts</title>

<style type="text/css">
table.table-hover tbody tr:hover {
	background-color: #e8ebef;
}
</style>

</head>

<body>
	<%@ include file="topmenu.jsp"%>
	<div class="container-fluid">
		<div class="row">
			<%@ include file="leftmenu.jsp"%>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">Concerts</h1>
				<div class="row">
					<div class="col-md-12">
						<a href='<c:url value="/concerts/concert/0" />'
							class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;Add </a>
						<a href='<c:url value="/concerts/csv" />' class="btn btn-info">
							<i class="fa fa-download"></i>&nbsp;Export do CSV
						</a>
					</div>
				</div>

				<div class="table-responsive">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th class="col-md-1">Id</th>
								<th class="col-md-2">Date</th>
								<th class="col-md-4">Description</th>
								<th class="col-md-3">Place</th>
								<th class="col-md-2">Action</th>
							</tr>
						</thead>

						<tbody>
							<c:forEach var="concert" items="${concerts}">
								<tr>
									<td><c:out value="${concert.idConcerts}" /></td>
									<td><fmt:formatDate value="${concert.date}"
											pattern="dd. MM. YYYY" /></td>
									<td><c:out value="${concert.description}" /></td>
									<td><c:out value="${concert.place}" /></td>
									<td class="text-center"><a class="btn btn-warning"
										href="<c:url value="/concerts/concert/${concert.idConcerts}"></c:url>"><i
											class="fa fa-pencil-square-o"></i>&nbsp;Edit</a> <a
										onclick="return confirm('Do you really want to delete this item?');"
										href="<c:url value="/concerts/concert/delete/${concert.idConcerts}"></c:url>"
										class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;Delete</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>

					<p>Number of records: ${numberConcerts}</p>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>