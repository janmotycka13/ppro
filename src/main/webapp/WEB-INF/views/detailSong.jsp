<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page contentType="text/html; charset=ISO-8859-1"%>

<html>
<head>
<%@ include file="head.jsp"%>

<title>Song</title>

</head>

<body>
<%@ include file="topmenu.jsp"%>
	<div class="container-fluid">
		<div class="row">
			<%@ include file="leftmenu.jsp"%>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">Detail song</h1>
				<p><b>Author:</b> ${song.author}</p>
				<p><b>Name:</b> ${song.name}</p>
				<button type="button" name="back" onclick="history.back()" class="btn btn-primary">Back</button>

				<a onclick="return confirm('Do you really want to delete this item?');"
					href="<c:url value="/songs/song/delete/${song.id}"></c:url>"
					class="btn btn-danger"> <i class="fa fa-trash"></i>&nbsp;Delete
				</a>
				
				<a class="btn btn-warning" href="<c:url value="/songs/song/${song.id}"></c:url>">
											<i class="fa fa-pencil-square-o"></i>&nbsp;Edit</a>

			</div>
		</div>
	</div>
</body>

</html>