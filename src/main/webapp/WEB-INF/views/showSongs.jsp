<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@ page contentType="text/html; charset=ISO-8859-1"%>
<html>

<head>
<%@ include file="head.jsp"%>
<title>Playlist</title>

<style type="text/css">
table.table-hover tbody tr:hover {
	background-color: #e8ebef;
}
</style>

</head>

<body>
	<%@ include file="topmenu.jsp"%>

	<div class="container-fluid">
		<div class="row">
			<%@ include file="leftmenu.jsp"%>
			
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">Playlist</h1>
				<div class="row">
					<div class="col-md-12">
						<a href='<c:url value="/songs/song/0" />' class="btn btn-success">
							<i class="fa fa-plus"></i>&nbsp;Add</a> 
							
						<a href='<c:url value="/songs/csv" />' class="btn btn-info">
							<i class="fa fa-download"></i>&nbsp;Export do CSV</a>
					</div>
				</div>
				
				<h2 id="demo"></h2>
				<script>
					var text;
					if ("${pocet}" == 0) {
					document.getElementById("demo").innerHTML = "Neni k dispozici zadny obsah"};
				</script>
				

				<div class="table-responsive">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th class="col-md-2">Name</th>
								<th class="col-md-3">Author</th>
								<th class="col-md-3">id</th>
								<th class="col-md-4">Action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="song" items="${songs}">
								<tr>
									<td><c:out value="${song.name}" /></td>
									<td><c:out value="${song.author}" /></td>
									<td><c:out value="${song.id}" /></td>
									
									<td class="text-center">
									
										<a class="btn btn-warning" href="<c:url value="/songs/song/${song.id}"></c:url>">
											<i class="fa fa-pencil-square-o"></i>&nbsp;Edit</a>&nbsp;
									
										<a onclick="return confirm('Do you really want to delete this item?');"
											href="<c:url value="/songs/song/delete/${song.id}"></c:url>"
											class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;Delete</a>

										<a class="btn btn-default" href="<c:url value="/songs/song/detail/${song.id}"></c:url>">
											<i class="fa fa-pencil-square-o"></i>&nbsp;Detail</a>&nbsp;
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<p>Number of records: ${pocet}</p>
<%-- 			<p>Max. ID: ${maximalniID}</p> --%>
			</div>
		</div>
	</div>
</body>
</html>