<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">

			<a class="navbar-brand" href="welcome"> <img alt="Brand"
				src="https://upload.wikimedia.org/wikipedia/commons/e/e6/Green_Day_%C2%A1Tr%C3%A9%21_Logo.png"
				class="img-responsive" width="80" />
			</a>
		</div>

		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
				<li>
				<a href="<c:url value="/logout"></c:url>"><i class="fa fa-sign-out"></i>&nbsp;Logout</a>
				</li>
			</ul>
		</div>

	</div>
</nav>