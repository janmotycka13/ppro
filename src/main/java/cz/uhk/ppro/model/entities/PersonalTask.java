package cz.uhk.ppro.model.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "personal_tasks")
public class PersonalTask {
	
	@Id
	@Column(name = "idPersonalTask")
	@GeneratedValue
	private int idPersonalTask;
	
	@Column(name = "date", nullable = false)
	private Date date;
	
	@Column(name = "taskTo", nullable = false)
	private String taskTo;
	
	@Column(name = "message", nullable = false)
	private String message;

	public int getIdPersonalTask() {
		return idPersonalTask;
	}

	public void setIdPersonalTask(int idPersonalTask) {
		this.idPersonalTask = idPersonalTask;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTaskTo() {
		return taskTo;
	}

	public void setTaskTo(String taskTo) {
		this.taskTo = taskTo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
