package cz.uhk.ppro.model.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "concerts")
public class Concert {

	@Id
	@Column(name = "idConcerts")
	@GeneratedValue
	private int idConcerts;

	@Column(name = "date", nullable = false)
	private Date date;

	@Column(name = "description", nullable = false)
	private String description;

	@Column(name = "place", nullable = false)
	private String place;

	public int getIdConcerts() {
		return idConcerts;
	}

	public void setIdConcerts(int idConcerts) {
		this.idConcerts = idConcerts;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}
}