package cz.uhk.ppro.model.components;

import javax.validation.constraints.*;

public class SongForm {

	@NotNull
	private String author;

	@NotNull
	@Min(3)
	private String name;

	@NotNull
	private Integer id;

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

//	@Override
//	public String toString() {
//		return "SongForm [author=" + author + ", name=" + name + ", id=" + id + "]";
//	}
}