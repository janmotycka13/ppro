package cz.uhk.ppro.model.components;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import cz.uhk.ppro.model.entities.Concert;

@Component
public class ConcertValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Concert.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "date", "error.date", "Date is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "place", "error.place", "Place is required.");
	}
}