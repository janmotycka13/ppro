package cz.uhk.ppro.model.components;

import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;

public class LoginForm {

	@NotNull
	@Email
	@Transient
	private String username;
	
	@NotNull
	@Transient
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}