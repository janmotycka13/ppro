package cz.uhk.ppro.model.components;

import java.util.Date;

import javax.validation.constraints.NotNull;

public class TaskForm {

	@NotNull
	private int idTasks;

	@NotNull
	private String name;
	
	private String description;
	
	private String place;
	
	@NotNull
	private Date dateOfEntry;
	
	@NotNull
	private Date dateOfCompletion;

	public int getIdTasks() {
		return idTasks;
	}

	public void setIdTasks(int idTasks) {
		this.idTasks = idTasks;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Date getDateOfEntry() {
		return dateOfEntry;
	}

	public void setDateOfEntry(Date dateOfEntry) {
		this.dateOfEntry = dateOfEntry;
	}

	public Date getDateOfCompletion() {
		return dateOfCompletion;
	}

	public void setDateOfCompletion(Date dateOfCompletion) {
		this.dateOfCompletion = dateOfCompletion;
	}

//	@Override
//	public String toString() {
//		return "TaskForm [idTasks=" + idTasks + ", name=" + name + ", description=" + description + ", place=" + place
//				+ ", dateOfEntry=" + dateOfEntry + ", dateOfCompletion=" + dateOfCompletion + "]";
//	}
	
}
