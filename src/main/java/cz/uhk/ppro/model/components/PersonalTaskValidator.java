package cz.uhk.ppro.model.components;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import cz.uhk.ppro.model.entities.PersonalTask;


public class PersonalTaskValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return PersonalTask.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "date", "error.date", "Date is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "taskTo", "error.taskTo", "Task-to is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "message", "error.message", "Message is required.");
	}
}
