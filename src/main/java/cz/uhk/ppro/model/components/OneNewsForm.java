package cz.uhk.ppro.model.components;

import javax.validation.constraints.*;

public class OneNewsForm {

	@NotNull
	private Integer idNews;

	@NotNull
	private String title;

	@NotNull
	private String content;

	public Integer getIdNews() {
		return idNews;
	}

	public void setIdNews(Integer idNews) {
		this.idNews = idNews;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

//	@Override
//	public String toString() {
//		return "OneNewsForm [title=" + title + ", content=" + content + ", idNews=" + idNews + "]";
//	}
}