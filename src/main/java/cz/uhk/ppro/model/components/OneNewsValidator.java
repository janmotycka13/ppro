package cz.uhk.ppro.model.components;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import cz.uhk.ppro.model.entities.OneNews;

@Component
public class OneNewsValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return OneNews.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "error.title", "Title is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "content", "error.content", "Content is required.");
	}
}