package cz.uhk.ppro.model.components;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import cz.uhk.ppro.model.entities.Task;

@Component
public class TaskValidator implements Validator{
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Task.class.isAssignableFrom(clazz);
	}
	
	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "error.name", "Name is required.");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dateOfEntry", "error.dateOfEntry", "dateOfEntry is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dateOfCompletion", "error.dateOfCompletion", "dateOfCompletion is required.");
	}

}
