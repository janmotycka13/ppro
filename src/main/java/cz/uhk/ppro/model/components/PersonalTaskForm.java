package cz.uhk.ppro.model.components;

import java.util.Date;

import javax.validation.constraints.NotNull;

public class PersonalTaskForm {
	
	@NotNull
	private int idPersonalTasks;

	@NotNull
	private Date date;
	
	@NotNull
	private String taskTo;
	
	@NotNull
	private String message;

	public int getIdPersonalTasks() {
		return idPersonalTasks;
	}

	public void setIdPersonalTasks(int idPersonalTasks) {
		this.idPersonalTasks = idPersonalTasks;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTaskTo() {
		return taskTo;
	}

	public void setTaskTo(String taskTo) {
		this.taskTo = taskTo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

//	@Override
//	public String toString() {
//		return "PersonalTaskForm [idPersonalTask=" + idPersonalTasks + ", date=" + date + ", taskTo=" + taskTo
//				+ ", message=" + message + "]";
//	}
	
}
