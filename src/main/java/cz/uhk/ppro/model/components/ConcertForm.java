package cz.uhk.ppro.model.components;

import java.util.Date;

import javax.validation.constraints.*;

public class ConcertForm {

	@NotNull
	private Integer idConcerts;

	@NotNull
	private Date date;

	@NotNull
	private String description;

	@NotNull
	private String place;

	public Integer getIdConcerts() {
		return idConcerts;
	}

	public void setIdConcerts(Integer idConcerts) {
		this.idConcerts = idConcerts;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

//	@Override
//	public String toString() {
//		return "ConcertForm [date=" + date + ", description=" + description + ", place=" + place + ", idConcerts="
//				+ idConcerts + "]";
//	}
}