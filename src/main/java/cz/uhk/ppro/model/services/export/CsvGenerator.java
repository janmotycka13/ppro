package cz.uhk.ppro.model.services.export;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import cz.uhk.ppro.model.entities.Concert;
import cz.uhk.ppro.model.entities.OneNews;
import cz.uhk.ppro.model.entities.PersonalTask;
import cz.uhk.ppro.model.entities.Song;
import cz.uhk.ppro.model.entities.Task;
import cz.uhk.ppro.model.entities.User;
import cz.uhk.ppro.model.daos.ConcertsDao;
import cz.uhk.ppro.model.daos.NewsDao;
import cz.uhk.ppro.model.daos.PersonalTasksDao;
import cz.uhk.ppro.model.daos.SongsDao;
import cz.uhk.ppro.model.daos.TasksDao;
import cz.uhk.ppro.model.daos.UserDao;

public class CsvGenerator {

	private void fillResponseCsv(HttpServletResponse response, List<ArrayList<String>> data) throws IOException {

		response.setContentType("text/csv");
		String fileName = "csv_export.csv";
		response.setHeader("Content-disposition", String.format("attachment;filename=%s", fileName));

		List<String> finalRows = new ArrayList<String>();

		for (ArrayList<String> row : data) {

			String newLine = null;

			for (String string : row) {

				if (newLine == null) {
					newLine = string;
				} else {
					newLine += ";" + string;
				}
			}

			newLine += "\n";
			finalRows.add(newLine);
		}

		Iterator<String> iter = finalRows.iterator();
		while (iter.hasNext()) {
			String outputString = (String) iter.next();
			response.getOutputStream().print(outputString);
		}

		response.getOutputStream().flush();
	}

	public void prepareSongsResponse(HttpServletResponse response) throws IOException {
		List<Song> list = new SongsDao().getItems();

		List<ArrayList<String>> data = new ArrayList<ArrayList<String>>();

		for (Song item : list) {
			ArrayList<String> row = new ArrayList<String>();

			row.add(String.valueOf(item.getId()));
			row.add(item.getAuthor());
			row.add(item.getName());

			data.add(row);
		}

		fillResponseCsv(response, data);
	}

	public void prepareConcertsResponse(HttpServletResponse response) throws IOException {
		List<Concert> list = new ConcertsDao().getItems();

		List<ArrayList<String>> data = new ArrayList<ArrayList<String>>();
		

		for (Concert item : list) {
			ArrayList<String> row = new ArrayList<String>();

			row.add(String.valueOf(item.getIdConcerts()));
			row.add(String.valueOf(new SimpleDateFormat("dd. MM. yyyy").format(item.getDate())));
			row.add(item.getDescription());
			row.add(item.getPlace());

			data.add(row);
		}

		fillResponseCsv(response, data);
	}

	public void prepareNewsResponse(HttpServletResponse response) throws IOException {
		List<OneNews> list = new NewsDao().getItems();

		List<ArrayList<String>> data = new ArrayList<ArrayList<String>>();

		for (OneNews item : list) {
			ArrayList<String> row = new ArrayList<String>();

			row.add(String.valueOf(item.getIdNews()));
			row.add(item.getTitle());
			row.add(item.getContent());

			data.add(row);
		}

		fillResponseCsv(response, data);
	}

	public void prepareUsersResponse(HttpServletResponse response) throws IOException {
		List<User> list = new UserDao().getItems();

		List<ArrayList<String>> data = new ArrayList<ArrayList<String>>();

		for (User item : list) {
			ArrayList<String> row = new ArrayList<String>();

			row.add(String.valueOf(item.getId()));
			row.add(item.getUsername());
			row.add(item.getName());
			row.add(item.getSurname());

			data.add(row);
		}
		fillResponseCsv(response, data);
	}
	
	public void prepareTasksResponse(HttpServletResponse response) throws IOException {
		List<Task> list = new TasksDao().getItems();

		List<ArrayList<String>> data = new ArrayList<ArrayList<String>>();

		for (Task item : list) {
			ArrayList<String> row = new ArrayList<String>();

			row.add(String.valueOf(item.getIdTask()));
			row.add(String.valueOf(item.getName()));
			row.add(String.valueOf(item.getDescription()));
			row.add(String.valueOf(item.getPlace()));
			row.add(String.valueOf(new SimpleDateFormat("dd. MM. yyyy").format(item.getDateOfEntry())));
			row.add(String.valueOf(new SimpleDateFormat("dd. MM. yyyy").format(item.getDateOfCompletion())));
			data.add(row);
		}
		fillResponseCsv(response, data);
	}
	
	public void preparePersonalTasksResponse(HttpServletResponse response) throws IOException {
		List<PersonalTask> list = new PersonalTasksDao().getItems();

		List<ArrayList<String>> data = new ArrayList<ArrayList<String>>();

		for (PersonalTask item : list) {
			ArrayList<String> row = new ArrayList<String>();

			row.add(String.valueOf(item.getIdPersonalTask()));
			row.add(String.valueOf(new SimpleDateFormat("dd. MM. yyyy").format(item.getDate())));
			row.add(String.valueOf(item.getTaskTo()));
			row.add(String.valueOf(item.getMessage()));
			data.add(row);
		}
		fillResponseCsv(response, data);
	}

}