package cz.uhk.ppro.model.daos;

import java.util.List;

import javax.persistence.EntityManager;

import cz.uhk.ppro.model.entities.PersonalTask;

public class PersonalTasksDao extends Service{
	
	public PersonalTasksDao() {
		super();
	}
	
	public List<PersonalTask> getItems() {
		EntityManager entityManager = sessionFactory.createEntityManager();
		entityManager.getTransaction().begin();
		
		List<PersonalTask> personalTasks = entityManager.createQuery("FROM PersonalTask", PersonalTask.class).getResultList();
		
		entityManager.getTransaction().commit();
		entityManager.close();
		
		return personalTasks;
	}
	
	private void save(PersonalTask p) {
		// TODO Auto-generated method stub

		EntityManager entityManager = sessionFactory.createEntityManager();
		entityManager.getTransaction().begin();
		
		if (p.getIdPersonalTask() == 0)
			entityManager.persist(p);
		else
			entityManager.merge(p);

		entityManager.getTransaction().commit();
		entityManager.close();
	}
	
	public void add(PersonalTask p) {
		save(p);
	}
	
	public void update(PersonalTask p) {
		PersonalTask personalTask = get(p.getIdPersonalTask());
		personalTask.setDate(p.getDate());
		personalTask.setTaskTo(p.getTaskTo());
		personalTask.setMessage(p.getMessage());
		
		save(personalTask);
	}
	
	public PersonalTask get(int idPersonalTask) {
		EntityManager entityManager = sessionFactory.createEntityManager();
		return entityManager.find(PersonalTask.class, idPersonalTask);
	}

	public void delete(int idPersonalTask) {
		// TODO Auto-generated method stub
		EntityManager entityManager = sessionFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.remove(get(idPersonalTask));
		entityManager.getTransaction().commit();
		entityManager.close();

	}
}
