package cz.uhk.ppro.model.daos;

import java.util.List;

import javax.persistence.EntityManager;

import cz.uhk.ppro.model.entities.Task;

public class TasksDao extends Service {

	public TasksDao() {
		super();
	}

	public List<Task> getItems() {
		EntityManager entityManager = sessionFactory.createEntityManager();
		entityManager.getTransaction().begin();
		List<Task> tasks = entityManager.createQuery("FROM Task", Task.class).getResultList();
		entityManager.getTransaction().commit();
		entityManager.close();
		return tasks;
	}

	// getId() == 0 -> pak add, jinak edit

	private void save(Task t) {
		EntityManager entityManager = sessionFactory.createEntityManager();
		entityManager.getTransaction().begin();

		if (t.getIdTask() == 0)
			entityManager.persist(t);
		else
			entityManager.merge(t);

		entityManager.getTransaction().commit();
		entityManager.close();
	}
	
	public void add(Task t) {
		save(t);
	}
	
	public void update(Task t) {
		Task task = get(t.getIdTask());
		task.setName(t.getName());
		task.setDescription(t.getDescription());
		task.setPlace(t.getPlace());
		task.setDateOfEntry(t.getDateOfEntry());
		task.setDateOfCompletion(t.getDateOfCompletion());
		
		save(task);
	}
	
	public Task get(int idTask) {
		EntityManager entityManager = sessionFactory.createEntityManager();
		return entityManager.find(Task.class, idTask);
	}
	
	public void delete(int idTask) {
		EntityManager entityManager = sessionFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.remove(get(idTask));
		entityManager.getTransaction().commit();
		entityManager.close();
	}
	
}
