package cz.uhk.ppro.model.daos;

import java.util.List;

import javax.persistence.EntityManager;

import cz.uhk.ppro.model.entities.Song;

public class SongsDao extends Service {
	
	public SongsDao() {
		super();
	}

	public List<Song> getItems() {
		EntityManager entityManager = sessionFactory.createEntityManager();
		entityManager.getTransaction().begin();
		final String q = "FROM Song ORDER by name";
		List<Song> songs = entityManager.createQuery(q, Song.class).getResultList();

		entityManager.getTransaction().commit();
		entityManager.close();

		return songs;
	}

	private void save(Song s) {
		EntityManager entityManager = sessionFactory.createEntityManager();
		entityManager.getTransaction().begin();

		if (s.getId() == 0)
			entityManager.persist(s);
		else
			entityManager.merge(s);

		entityManager.getTransaction().commit();
		entityManager.close();
	}

	public void add(Song s) {
		save(s);
	}

	public void update(Song s) {
		Song song = get(s.getId());
		song.setAuthor(s.getAuthor());
		song.setName(s.getName());
		save(song);
	}

	public Song get(int id) {
		EntityManager entityManager = sessionFactory.createEntityManager();

		return entityManager.find(Song.class, id);
	}

	public void delete(int id) {
		EntityManager entityManager = sessionFactory.createEntityManager();
		entityManager.getTransaction().begin();
		entityManager.remove(get(id));
		entityManager.getTransaction().commit();
		entityManager.close();
	}
}