package cz.uhk.ppro.model.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import cz.uhk.ppro.model.entities.User;
import cz.uhk.ppro.model.daos.Service;
import javax.servlet.http.HttpSession;

public class UserDao extends Service {

	public UserDao() {
		super();
	}

	/**
	 * @param username
	 * @return User if exists or null if not exists
	 */
	public User existsUser(String username) {

		EntityManager entityManager = sessionFactory.createEntityManager();

		entityManager.getTransaction().begin();
		User u;

		try {
			u = (User) entityManager.createQuery("FROM User u WHERE u.username = :username")
					.setParameter("username", username).getSingleResult();
		} catch (NoResultException ex) {
			u = null;
		}

		entityManager.getTransaction().commit();
		entityManager.close();

		return u;
	}

	public List<User> getItems() {
		EntityManager entityManager = sessionFactory.createEntityManager();
		entityManager.getTransaction().begin();
		List<User> users = entityManager.createQuery("FROM User", User.class).getResultList();

		entityManager.getTransaction().commit();
		entityManager.close();

		return users;
	}

	public static String getLoggedUserName(HttpSession session) {
		return (String) session.getAttribute("username");
	}

	public static boolean isUserLogged(HttpSession session) {
		String u = (String) session.getAttribute("username");

		return u != null;
	}

	public static void login(HttpSession session, String username) {
		session.setAttribute("username", username);
	}

	public static void logout(HttpSession session) {
		session.removeAttribute("username");
	}
}