package cz.uhk.ppro.model.daos;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class Service {

	protected SessionFactory sessionFactory;

	private void setUp() {
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();

		try {

			sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
		}

		catch (Exception e) {
			sessionFactory = null;
			StandardServiceRegistryBuilder.destroy(registry);
		}
	}

	public Service() {
		setUp();
	}
}