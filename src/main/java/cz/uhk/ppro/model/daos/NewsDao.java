package cz.uhk.ppro.model.daos;

import java.util.List;
import javax.persistence.EntityManager;

import cz.uhk.ppro.model.entities.OneNews;

public class NewsDao extends Service {

	public NewsDao() {
		super();
	}

	public List<OneNews> getItems() {
		EntityManager entityManager = sessionFactory.createEntityManager();
		entityManager.getTransaction().begin();
		List<OneNews> news = entityManager.createQuery("FROM OneNews", OneNews.class).getResultList();

		entityManager.getTransaction().commit();
		entityManager.close();
		
		return news;
	}

	private void save(OneNews n) {
		EntityManager entityManager = sessionFactory.createEntityManager();
		entityManager.getTransaction().begin();

		if (n.getIdNews() == 0)
			entityManager.persist(n);
		else
			entityManager.merge(n);

		entityManager.getTransaction().commit();
		entityManager.close();
	}

	public void add(OneNews n) {
		save(n);
	}

	public void update(OneNews n) {
		OneNews oneNews = get(n.getIdNews());
		oneNews.setTitle(n.getTitle());
		oneNews.setContent(n.getContent());
		save(oneNews);
	}

	public OneNews get(int idNews) {
		EntityManager entityManager = sessionFactory.createEntityManager();

		return entityManager.find(OneNews.class, idNews);
	}

	public void delete(int idNews) {
		EntityManager entityManager = sessionFactory.createEntityManager();
		entityManager.getTransaction().begin();

		entityManager.remove(get(idNews));

		entityManager.getTransaction().commit();
		entityManager.close();
	}
}