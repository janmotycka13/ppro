package cz.uhk.ppro.model.daos;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import cz.uhk.ppro.model.entities.Concert;

@Repository
public class ConcertsDao extends Service {

	public ConcertsDao() {
		super();
	}

	public List<Concert> getItems() {
		EntityManager entityManager = sessionFactory.createEntityManager();
		entityManager.getTransaction().begin();
		List<Concert> concerts = entityManager.createQuery("FROM Concert ORDER by date DESC", Concert.class).getResultList();

		entityManager.getTransaction().commit();
		entityManager.close();

		return concerts;
	}

	/**
	 * Pokud ma s.getId() == 0 pak se pridava, jinak se edituje
	 * 
	 * @param s
	 */
	private void save(Concert c) {
		EntityManager entityManager = sessionFactory.createEntityManager();
		entityManager.getTransaction().begin();

		if (c.getIdConcerts() == 0)
			entityManager.persist(c);
		else
			entityManager.merge(c);

		entityManager.getTransaction().commit();
		entityManager.close();
	}

	public void add(Concert c) {
		save(c);
	}

	public void update(Concert c) {
		Concert concert = get(c.getIdConcerts());
		concert.setDate(c.getDate());
		concert.setDescription(c.getDescription());
		concert.setPlace(c.getPlace());

		save(concert);
	}

	public Concert get(int idConcerts) {
		EntityManager entityManager = sessionFactory.createEntityManager();

		return entityManager.find(Concert.class, idConcerts);
	}

	public void delete(int idConcerts) {
		EntityManager entityManager = sessionFactory.createEntityManager();
		entityManager.getTransaction().begin();

		entityManager.remove(get(idConcerts));

		entityManager.getTransaction().commit();
		entityManager.close();
	}
}