package cz.uhk.ppro.controllers;

import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cz.uhk.ppro.model.daos.UserDao;

@Controller
public class WelcomeController {
	
	@RequestMapping(value="/welcome")
	public String welcome(Locale locale, Model model, HttpSession session) {
		if (!UserDao.isUserLogged(session)) return "redirect:/";
		
		String username = (String) session.getAttribute("username");
		//System.out.println(username);
		model.addAttribute("name", username);
		
		return "welcome";
	}
}