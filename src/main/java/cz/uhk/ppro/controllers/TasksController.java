package cz.uhk.ppro.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

import cz.uhk.ppro.model.components.TaskForm;
import cz.uhk.ppro.model.components.TaskValidator;
import cz.uhk.ppro.model.entities.Task;
import cz.uhk.ppro.model.daos.TasksDao;
import cz.uhk.ppro.model.daos.UserDao;
import cz.uhk.ppro.model.services.export.CsvGenerator;

@Controller
@RequestMapping(value = "/tasks")
public class TasksController {
	TaskValidator validator = new TaskValidator();
	
	@RequestMapping(value = "/csv", method = RequestMethod.GET)
	public void getCsv(Locale locale, Model model, HttpSession session, HttpServletResponse response)
			throws IOException {
		new CsvGenerator().prepareTasksResponse(response);
	}
	
	@RequestMapping(value = "/tasks", method = RequestMethod.GET)
	public String showTasks(Locale locale, Model model, HttpSession session) {
		if (!UserDao.isUserLogged(session))
			return "redirect:/";
		
		List<Task> taskList = new TasksDao().getItems();
		model.addAttribute("tasks", taskList);
		model.addAttribute("numberTasks", taskList.size());
		
		return "showTasks";
	}
	
	@RequestMapping(value = "/task/{idTasks}", method = RequestMethod.GET)
	public String showTaskForm(Model model, @PathVariable("idTasks") String sIdTasks, HttpSession session) {
		if (!UserDao.isUserLogged(session))
			return "redirect:/";
		
		Integer idTasks;

		try {
			idTasks = Integer.parseInt(sIdTasks);
		} catch (NumberFormatException ex) {
			return "404";
		}

		TaskForm form = new TaskForm();
		form.setIdTasks(idTasks);
		
		if (idTasks > 0) {
			Task t = new TasksDao().get(idTasks);
			form.setName(t.getName());
			form.setDescription(t.getDescription());
			form.setPlace(t.getPlace());
			form.setDateOfEntry(t.getDateOfEntry());
			form.setDateOfCompletion(t.getDateOfCompletion());
		}
		model.addAttribute("taskForm", form);
		return "showTaskForm";
	}
	
	@RequestMapping(value = "/task/addEdit", method = RequestMethod.POST)
	public String submitTaskForm(@ModelAttribute("taskForm") TaskForm taskForm, BindingResult result, SessionStatus status) {
		validator.validate(taskForm, result);
		TasksDao repository = new TasksDao();
		
		if (result.hasErrors()) {
			return "showTaskForm";
		}
		
		Task t = new Task();
		t.setName(taskForm.getName());
		t.setDescription(taskForm.getDescription());
		t.setPlace(taskForm.getPlace());
		t.setDateOfEntry(taskForm.getDateOfEntry());
		t.setDateOfCompletion(taskForm.getDateOfCompletion());
		
		if (taskForm.getIdTasks() == 0) {
			repository.add(t);
		} else {
			repository.update(t);
		}
		
		return "redirect:/ tasks";
	
	}
	
	@RequestMapping(value = "/task/delete/{idTasks}", method = RequestMethod.GET)	
	public String deleteTask(Model model, @PathVariable("idTasks") String sIdTasks) {

		Integer idTasks;

		try {
			idTasks = Integer.parseInt(sIdTasks);
		} catch (NumberFormatException ex) {
			return "404";
		}

		new TasksDao().delete(idTasks);

		return "redirect:/tasks";
	}

}
