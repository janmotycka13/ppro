package cz.uhk.ppro.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

import cz.uhk.ppro.model.components.OneNewsForm;
import cz.uhk.ppro.model.components.OneNewsValidator;
import cz.uhk.ppro.model.entities.OneNews;
import cz.uhk.ppro.model.daos.NewsDao;
import cz.uhk.ppro.model.daos.UserDao;
import cz.uhk.ppro.model.services.export.CsvGenerator;

@Controller
@RequestMapping(value = "/news")
public class NewsController {
	OneNewsValidator validator = new OneNewsValidator();

	@RequestMapping(value = "/csv", method = RequestMethod.GET)
	public void getCsv(Locale locale, Model model, HttpSession session, HttpServletResponse response)
			throws IOException {
		new CsvGenerator().prepareNewsResponse(response);
	}

	@RequestMapping(value = "/news", method = RequestMethod.GET)
	public String showNews(Locale locale, Model model, HttpSession session) {
		if (!UserDao.isUserLogged(session))
			return "redirect:/";
		
		List<OneNews> newsList = new NewsDao().getItems();
		model.addAttribute("news", newsList);
		model.addAttribute("numberNews", newsList.size());

		return "showNews";
	}

	@RequestMapping(value = "/oneNews/{idNews}", method = RequestMethod.GET)
	public String showOneNewsForm(Model model, @PathVariable("idNews") String sIdNews, HttpSession session) {
		if (!UserDao.isUserLogged(session))
			return "redirect: ";

		Integer idNews;

		try {
			idNews = Integer.parseInt(sIdNews);
		} catch (NumberFormatException ex) {
			return "404";
		}

		OneNewsForm form = new OneNewsForm();
		form.setIdNews(idNews);

		if (idNews > 0) {
			OneNews n = new NewsDao().get(idNews);
			form.setTitle(n.getTitle());
			form.setContent(n.getContent());
		}

		model.addAttribute("oneNewsForm", form);
		return "showOneNewsForm";
	}

	@RequestMapping(value = "/oneNews/addEdit", method = RequestMethod.POST)
	public String submitOneNewsForm(@ModelAttribute("oneNewsForm") OneNewsForm oneNewsForm, BindingResult result,
			SessionStatus status) {

		validator.validate(oneNewsForm, result);
		NewsDao repository = new NewsDao();

		if (result.hasErrors()) {
			return "showOneNewsForm";
		}

		OneNews n = new OneNews();
		n.setTitle(oneNewsForm.getTitle());
		n.setContent(oneNewsForm.getContent());
		n.setIdNews(oneNewsForm.getIdNews());

		if (oneNewsForm.getIdNews() == 0) {
			repository.add(n);
		} else {
			repository.update(n);
		}

		return "redirect:/news";
	}

	@RequestMapping(value = "/oneNews/delete/{idNews}", method = RequestMethod.GET)
	public String deleteOneNews(Model model, @PathVariable("idNews") String sIdNews) {

		Integer idNews;

		try {
			idNews = Integer.parseInt(sIdNews);
		} catch (NumberFormatException ex) {
			return "404";
		}

		new NewsDao().delete(idNews);

		return "redirect:/news";
	}
}