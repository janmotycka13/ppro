package cz.uhk.ppro.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

import cz.uhk.ppro.model.components.PersonalTaskForm;
import cz.uhk.ppro.model.components.PersonalTaskValidator;
import cz.uhk.ppro.model.entities.PersonalTask;
import cz.uhk.ppro.model.daos.PersonalTasksDao;
import cz.uhk.ppro.model.daos.UserDao;
import cz.uhk.ppro.model.services.export.CsvGenerator;

@Controller
@RequestMapping(value = "/personalTasks")
public class PersonalTasksController {
	PersonalTaskValidator validator = new PersonalTaskValidator();

	@RequestMapping(value = "/csv", method = RequestMethod.GET)
	public void getCsv(Locale locale, Model model, HttpSession session, HttpServletResponse response)
			throws IOException {
		new CsvGenerator().preparePersonalTasksResponse(response);
	}
	
	@RequestMapping(value = "/personalTasks", method = RequestMethod.GET)
	public String showPersonalTasks(Locale locale, Model model, HttpSession session) {
		if (!UserDao.isUserLogged(session))
			return "redirect:/";

		List<PersonalTask> personalTasksList = new PersonalTasksDao().getItems();
		model.addAttribute("personalTasks", personalTasksList);
		model.addAttribute("numberPersonalTasks", personalTasksList.size());
		
		return "showPersonalTasks";
	}
	
	@RequestMapping(value = "/personalTask/{idPersonalTasks}", method = RequestMethod.GET)
	public String showPersonalTaskForm(Model model, @PathVariable("idPersonalTasks") String sIdPersonalTasks, HttpSession session) {
		if (!UserDao.isUserLogged(session))
			return "redirect:/";

		Integer idPersonalTasks;

		try {
			idPersonalTasks = Integer.parseInt(sIdPersonalTasks);
		} catch (NumberFormatException ex) {
			return "404";
		}

		PersonalTaskForm form = new PersonalTaskForm();
		form.setIdPersonalTasks(idPersonalTasks);

		if (idPersonalTasks > 0) {
			PersonalTask p = new PersonalTasksDao().get(idPersonalTasks);
			form.setDate(p.getDate());
			form.setTaskTo(p.getTaskTo());
			form.setMessage(p.getMessage());
		}

		model.addAttribute("personalTaskForm", form);
		return "showPersonalTaskForm";
	}
	
	@RequestMapping(value = "/personalTask/addEdit", method = RequestMethod.POST)
	public String submitPersonalTaskForm(@ModelAttribute("personalTaskForm") PersonalTaskForm personalTaskForm, BindingResult result,
			SessionStatus status) {

		validator.validate(personalTaskForm, result);
		PersonalTasksDao repository = new PersonalTasksDao();

		if (result.hasErrors()) {
			return "showPersonalTaskForm";
		}

		PersonalTask p = new PersonalTask();
		p.setDate(personalTaskForm.getDate());
		p.setTaskTo(personalTaskForm.getTaskTo());
		p.setMessage(personalTaskForm.getMessage());
		
		if (personalTaskForm.getIdPersonalTasks() == 0) {
			repository.add(p);
		} else {
			repository.update(p);
		}
		return "redirect:/personalTasks";
	}
	
	@RequestMapping(value = "/personalTask/delete/{idPersonalTasks}", method = RequestMethod.GET)
	public String deletePersonalTask(Model model, @PathVariable("idPersonalTasks") String sIdPersonalTasks) {

		Integer idPersonalTasks;

		try {
			idPersonalTasks = Integer.parseInt(sIdPersonalTasks);
		} catch (NumberFormatException ex) {
			return "404";
		}

		new PersonalTasksDao().delete(idPersonalTasks);

		return "redirect:/personalTasks";
	}

}
