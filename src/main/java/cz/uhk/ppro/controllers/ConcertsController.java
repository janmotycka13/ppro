package cz.uhk.ppro.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

import cz.uhk.ppro.model.components.ConcertForm;
import cz.uhk.ppro.model.components.ConcertValidator;
import cz.uhk.ppro.model.entities.Concert;
import cz.uhk.ppro.model.daos.ConcertsDao;
import cz.uhk.ppro.model.daos.UserDao;
import cz.uhk.ppro.model.services.export.CsvGenerator;

@Controller
@RequestMapping(value = "/concerts")
public class ConcertsController {
	ConcertValidator validator = new ConcertValidator();

//	DI OVER ANOTATIONS
	@Inject
	private ConcertsDao concertsDao;

	
//	DI OVER SETTER	
//	private ConcertsDao concertsDao;
//	
//	@Autowired
//	public void setConcertsDao(ConcertsDao concertsDao) {
//		this.concertsDao = concertsDao;
//	}
	

//	DI OVER CONSTRUCTOR
//	private ConcertsDao concertsDao;
//
//	public ConcertsController() {
//		concertsDao = new ConcertsDao();
//	}
	

	@RequestMapping(value = "/csv", method = RequestMethod.GET)
	public void getCsv(Locale locale, Model model, HttpSession session, HttpServletResponse response)
			throws IOException {
		new CsvGenerator().prepareConcertsResponse(response);
	}

	@RequestMapping(value = "/concerts", method = RequestMethod.GET)
	public String showConcerts(Locale locale, Model model, HttpSession session) {
		if (!UserDao.isUserLogged(session))
			return "redirect:/";

		List<Concert> concertsList = concertsDao.getItems();
		model.addAttribute("concerts", concertsList);
		model.addAttribute("numberConcerts", concertsList.size());

		return "showConcerts";
	}

	@RequestMapping(value = "/concert/{idConcerts}", method = RequestMethod.GET)
	public String showConcertForm(Model model, @PathVariable("idConcerts") String sIdConcerts, HttpSession session) {
		if (!UserDao.isUserLogged(session))
			return "redirect:/";

		Integer idConcerts;

		try {
			idConcerts = Integer.parseInt(sIdConcerts);
		} catch (NumberFormatException ex) {
			return "404";
		}

		ConcertForm form = new ConcertForm();
		form.setIdConcerts(idConcerts);

		if (idConcerts > 0) {
			Concert c = concertsDao.get(idConcerts);
			form.setDate(c.getDate());
			form.setDescription(c.getDescription());
			form.setPlace(c.getPlace());
		}

		model.addAttribute("concertForm", form);
		return "showConcertForm";
	}

	@RequestMapping(value = "/concert/addEdit", method = RequestMethod.POST)
	public String submitConcertForm(@ModelAttribute("concertForm") ConcertForm concertForm, BindingResult result,
			SessionStatus status) {

		validator.validate(concertForm, result);

		if (result.hasErrors()) {
			return "showConcertForm";
		}

		Concert c = new Concert();
		c.setDate(concertForm.getDate());
		c.setDescription(concertForm.getDescription());
		c.setPlace(concertForm.getPlace());
		c.setIdConcerts(concertForm.getIdConcerts());

		if (concertForm.getIdConcerts() == 0) {
			concertsDao.add(c);
		} else {
			concertsDao.update(c);
		}

		return "redirect:/concerts";
	}

	@RequestMapping(value = "/concert/delete/{idConcerts}", method = RequestMethod.GET)
	public String deleteConcert(Model model, @PathVariable("idConcerts") String sIdConcerts) {

		Integer idConcerts;

		try {
			idConcerts = Integer.parseInt(sIdConcerts);
		} catch (NumberFormatException ex) {
			return "404";
		}

		concertsDao.delete(idConcerts);

		return "redirect:/concerts";
	}
}