package cz.uhk.ppro.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cz.uhk.ppro.model.entities.User;
import cz.uhk.ppro.model.daos.UserDao;
import cz.uhk.ppro.model.services.export.CsvGenerator;

@Controller
@RequestMapping(value = "/users")
public class UsersController {
	
	@RequestMapping(value = "/csv", method = RequestMethod.GET)
	public void getCsv(Locale locale, Model model, HttpSession session, HttpServletResponse response)
			throws IOException {
		new CsvGenerator().prepareUsersResponse(response);
	}

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String showUsers(Locale locale, Model model, HttpSession session) {
		if (!UserDao.isUserLogged(session))
			return "redirect:/";
		
		List<User> listUsers = new UserDao().getItems();
		model.addAttribute("users", listUsers);
		
		
		
		
		User u = listUsers.get(0);
		int max = u.getId();
		
		for (int i = 1; i < listUsers.size(); i++) {
			u = listUsers.get(i);
				if (u.getId() > max) max = u.getId();
		}
		model.addAttribute("maxID", max);
		System.out.println("Max ID:" + max);
		
		
		
		return "showUsers";
	}
}