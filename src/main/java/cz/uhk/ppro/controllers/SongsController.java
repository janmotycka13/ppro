package cz.uhk.ppro.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

import cz.uhk.ppro.model.components.SongForm;
import cz.uhk.ppro.model.components.SongValidator;
import cz.uhk.ppro.model.entities.Song;
import cz.uhk.ppro.model.daos.SongsDao;
import cz.uhk.ppro.model.daos.UserDao;
import cz.uhk.ppro.model.services.export.CsvGenerator;

@Controller
@RequestMapping(value = "/songs")
public class SongsController {
	SongValidator validator = new SongValidator();
	
//	@Autowired
//	SongsRepository rep;
	
	@RequestMapping(value = "/csv", method = RequestMethod.GET)
	public void getCsv(Locale locale, Model model, HttpSession session, HttpServletResponse response)
			throws IOException {
		new CsvGenerator().prepareSongsResponse(response);
	}

	@RequestMapping(value = "/songs", method = RequestMethod.GET)
	public String showSongs(Locale locale, Model model, HttpSession session) {
		if (!UserDao.isUserLogged(session))
			return "redirect:/";
		List<Song> seznamSongs =  new SongsDao().getItems();
		model.addAttribute("songs", seznamSongs);
		model.addAttribute("pocet", seznamSongs.size());
//		model.addAttribute("maximalniID", new SongsRepository().favouriteSong());
		return "showSongs";
	}

	@RequestMapping(value = "/song/{id}", method = RequestMethod.GET)
	public String showSongForm(Model model, @PathVariable("id") String sId, HttpSession session) {
		if (!UserDao.isUserLogged(session))
			return "redirect:/";

		Integer id;

		try {
			id = Integer.parseInt(sId);
		} catch (NumberFormatException ex) {
			return "404";
		}

		SongForm form = new SongForm();
		form.setId(id);

		if (id > 0) {
			Song s = new SongsDao().get(id);
			form.setAuthor(s.getAuthor());
			form.setName(s.getName());
		}

		model.addAttribute("songForm", form);
		return "showSongForm";
	}

	@RequestMapping(value = "/song/addEdit", method = RequestMethod.POST)
	public String submitSongForm(@ModelAttribute("songForm") SongForm songForm, BindingResult result,
			SessionStatus status) {

		validator.validate(songForm, result);
		SongsDao repository = new SongsDao();

		if (result.hasErrors()) {
			return "showSongForm";
		}

		Song s = new Song();
		s.setAuthor(songForm.getAuthor());
		s.setName(songForm.getName());
		s.setId(songForm.getId());

		if (songForm.getId() == 0) {
			repository.add(s);
		} else {
			repository.update(s);
		}

		return "redirect:/songs";
	}

	@RequestMapping(value = "/song/delete/{id}", method = RequestMethod.GET)
	public String deleteSong(Model model, @PathVariable("id") String sId) {

		Integer id;

		try {
			id = Integer.parseInt(sId);
		} catch (NumberFormatException ex) {
			return "404";
		}

		new SongsDao().delete(id);

		return "redirect:/songs";
	}
	
	@RequestMapping(value = "/song/detail/{id}", method = RequestMethod.GET)
	public String detailSong (Model model, @PathVariable("id") String sId, HttpSession session) {
		if (!UserDao.isUserLogged(session))
			return "redirect:/";
		
		Integer id;

		try {
			id = Integer.parseInt(sId);
		} catch (NumberFormatException ex) {
			return "404";
		}

		model.addAttribute("song", new SongsDao().get(id));
		return "detailSong";
	}
}