package cz.uhk.ppro.controllers;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

import cz.uhk.ppro.model.components.LoginForm;
import cz.uhk.ppro.model.components.LoginValidator;
import cz.uhk.ppro.model.entities.User;
import cz.uhk.ppro.model.daos.UserDao;

@Controller
@RequestMapping("/")
public class LoginController {

	LoginValidator validator = new LoginValidator();
	UserDao repository = new UserDao();

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String login(Locale locale, Model model, HttpSession session) {

		if (!UserDao.isUserLogged(session)) {
			LoginForm form = new LoginForm();

			model.addAttribute("loginForm", form);

			return "login";
		} else {
			return "redirect:/welcome";
		}

	}

	@RequestMapping(method = RequestMethod.POST)
	public String submitLoginForm(Model model, @ModelAttribute("loginForm") LoginForm loginForm, BindingResult result,
			SessionStatus status, HttpSession session, HttpServletRequest request, HttpServletResponse response) {

		validator.validate(loginForm, result);

		if (result.hasErrors()) {
			return "login";
		}

		User u = repository.existsUser(loginForm.getUsername());

		if (u != null) {

			boolean isPasswordValid = new Md5PasswordEncoder().encodePassword(loginForm.getPassword(), null)
					.equals(u.getPassword());

			if (isPasswordValid) {

				UserDao.login(session, u.getUsername());

				return "redirect:/welcome";

			} else {
				result.reject("msg", "Wrong password.");
				return "login";
			}

		} else {
			result.reject("msg", "User does not exist.");
			return "login";
		}
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpSession session) {
		UserDao.logout(session);
		return "redirect:/";
	}
}